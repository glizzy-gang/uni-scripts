#!/usr/bin/env python3

DOMAIN_TIMETABLE = "https://timetables.liverpool.ac.uk"
ENDP_LOGIN = "/account?returnUrl=/"
ENDP_EVENTS = "/services/get-events"
ENDP_WEEK_TITLE = "/services/get-week-title"
SESSION_COOKIE = "TimetableWebAuth"

from datetime import datetime, timedelta
import json
from http.server import BaseHTTPRequestHandler, HTTPServer
import os
import sys

import requests
from ical.calendar import Calendar
from ical.event import Event
from ical.calendar_stream import IcsCalendarStream

LOGIN_COOKIE = None
USER_NAME = os.getenv("TIMETABLE_USERNAME")
USER_PASS = os.getenv("TIMETABLE_PASSWORD")

if USER_NAME is None or len(USER_NAME) == 0:
    raise Exception("Must provide username")

if USER_PASS is None or len(USER_PASS) == 0:
    raise Exception("Must provide password")


def get_login_cookie(username: str, password: str):
    r = requests.post(
        DOMAIN_TIMETABLE + ENDP_LOGIN,
        data={
            "Username": username,
            "Password": password,
        },
        allow_redirects=False,
    )
    if r.status_code != 302:
        raise Exception(f"Login failed: {r.status_code} {r.reason}")
    print(f"Successfully logged in as {username}", file=sys.stderr)
    return r.cookies.get(SESSION_COOKIE)


def authenticated_request(method: str, url: str, **kwargs):
    global LOGIN_COOKIE
    kwargs["cookies"] = {SESSION_COOKIE: LOGIN_COOKIE}
    kwargs["allow_redirects"] = False
    r = requests.request(method, url, **kwargs)
    if r.status_code == 302 and r.headers["location"].startswith("/account"):
        LOGIN_COOKIE = get_login_cookie(USER_NAME, USER_PASS)
        return authenticated_request(method, url, **kwargs)
    print(
        f"Finished request to \"{url}\" {r.status_code} {r.reason} {r.headers['Content-Type']}",
        file=sys.stderr,
    )
    return r


def get_events(start: datetime, end: datetime):
    r = authenticated_request(
        "GET",
        DOMAIN_TIMETABLE + ENDP_EVENTS,
        params={
            "start": start.isoformat(),
            "end": end.isoformat(),
        },
    )

    return r.json()


def get_weeks_calendar(weeks: int):
    today = datetime.today()
    start_of_week = today - timedelta(days=today.weekday())
    end_of_week = start_of_week + timedelta(days=7 * weeks - 1)
    events = get_events(start_of_week, end_of_week)

    calendar = Calendar()
    for event in events:
        calendar.events.append(
            Event(
                summary=event["activitydesc"],
                start=datetime.strptime(event["start"], "%Y-%m-%dT%H:%M"),
                end=datetime.strptime(event["end"], "%Y-%m-%dT%H:%M"),
                location=event["locationdesc"],
                categories=[event["title"]],
                uid=event["uniqueid"],
                description=f"{event['activityname']}\nCode: {event['attendancecode']}",
            )
        )
    return calendar


class RequestHandler(BaseHTTPRequestHandler):
    def do_GET(self):
        try:
            cal = get_weeks_calendar(2)
        except Exception as e:
            print("Error " + e, file=sys.stderr)
            self.send_response(500)
            self.end_headers()
            return
        cal_text = IcsCalendarStream.calendar_to_ics(cal).encode()
        self.send_response(200)
        self.send_header("Content-Type", "text/calendar")
        self.send_header("Content-Length", len(cal_text))
        self.end_headers()
        self.wfile.write(cal_text)


if __name__ == "__main__":
    HOST = os.getenv("HOST", "0.0.0.0")
    PORT = int(os.getenv("PORT", 8000))
    httpd = HTTPServer((HOST, PORT), RequestHandler)
    print(f"Listening on {HOST}:{PORT}")
    httpd.serve_forever()
