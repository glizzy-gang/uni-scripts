#!/bin/sh

# Download COMP122 labs
# Requirements: rofi freerdp

missing() {
  echo "Missing dependency $1"
  echo "Install dependecies with one of the below commands (dependent on distro):"
  echo "\tsudo pacman -S --needed rofi freerdp"
  echo "\tsudo apt-get install rofi freerdp2-x11"
  exit 1
}

command -v rofi >/dev/null || missing "rofi"
command -v xfreerdp >/dev/null || missing "freerdp"

pc="$1"
if [ "$pc" = "" ]; then
  pc=$(rofi -p "Enter PC name (e.g. E001)" -dmenu -lines 0)
fi

xfreerdp /u:"LIVAD\\$(rofi -p "Username" -dmenu -lines 0)" /v:"$pc" /cert:ignore /g:rdgateway.liv.ac.uk:443 /dynamic-resolution /kbd-lang:0x0809 /p:"$(rofi -p "Password" -password -dmenu -lines 0)"

if [ $? -ne 0 ]; then
  dunstify -a "xfreerdp" -u critical "RDP" "Error connecting to $1"
fi
