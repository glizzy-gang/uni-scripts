#!/bin/sh

# Your canvas API token https://liverpool.instructure.com/profile/settings
TOKEN="$(cat ~/.config/canvas_token)"

courses=$(curl -sH "Authorization: Bearer ${TOKEN}" "https://liverpool.instructure.com/api/v1/courses?enrollment_state=active&state=available&per_page=50")
course_number=$(echo "$courses" | jq -r ".[] | .name" | rofi -dmenu -format i -p "Course")
if [ "$course_number" = "" ]; then
  echo "No course was selected" >&2
  exit 0
fi
course=$(printf "%s" "$courses" | jq -r ".[$course_number] | .id")
echo "Course: $course"

assignments=$(curl -sH "Authorization: Bearer ${TOKEN}" "https://liverpool.instructure.com/api/v1/courses/${course}/assignments?per_page=15&order_by=due_at")
echo "$assignments"
assignment_number=$(printf "%s" "$assignments" | jq -r ".[] | .name" | rofi -dmenu -format i -p "Assignment")
if [ "$assignment_number" = "" ]; then
  echo "No assignment selected" >&2
  exit 0
fi
assignment=$(printf "%s" "$assignments" | jq -r ".[$assignment_number] | .id")

# Fetch and launch external tool params
url=$(curl -s -H "Authorization: Bearer ${TOKEN}" "https://liverpool.instructure.com/api/v1/courses/${course}/assignments/${assignment}" | jq -r ".url")
sessionless=$(curl -Ls -H "Authorization: Bearer ${TOKEN}" "${url}" | jq -r ".url")

# Now open sessionless in your browser for full page code grade
echo "${sessionless}"
xdg-open "${sessionless}"
