# Comp122 scripts

## download\_labs

By default this script will open `README.md` in your default text editor. However if you have `pandoc` and `texlive-core` installed `README.md` will be converted to a PDF and opened in your default PDF viewer.
