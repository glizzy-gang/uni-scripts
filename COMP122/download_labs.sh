#!/bin/sh

# Download COMP122 labs
# Requirements: curl jq fzf
# sudo pacman -S --needed curl jq fzf

missing() {
  echo "Missing dependency $1"
  echo "Install dependecies with one of the below commands (dependent on distro):"
  echo "\tsudo pacman -S --needed curl jq fzf"
  echo "\tsudo apt-get install curl jq fzf"
  exit 1
}

command -v jq >/dev/null || missing "jq"
command -v curl >/dev/null || missing "curl"
command -v fzf >/dev/null || missing "fzf"

REPO="https://api.github.com/repos/liv-ac-uk/comp122/"

download_dir() {
  mkdir "$2"
  cd "$2"
  contents=$(curl -s "${REPO}contents$1$2" | jq -r ".[] | @json")
  for content in $contents; do
    ftype=$(echo -n "$content" | jq -r ".type")
    if [ "$ftype" = "file" ]; then
      name=$(echo -n "$content" | jq -r ".name")
      if [ "$name" != ".cs50.yml" ]; then
        url=$(echo -n "$content" | jq -r ".download_url")
        echo "Downloading $name"
        curl -sO "$url"
      fi
    elif [ "$ftype" = "dir" ]; then
      path=$(echo -n "$content" | jq -r ".name")
      download_dir "$1$2/" "$path"
    fi
  done
  cd ..
}

labs=$(curl -s "${REPO}contents/labs" | jq -r ".[] | select(.type == \"dir\") | .name")

lab=$(echo "$labs" | fzf --header="Choose lab to download")

if [ $? -eq 0 ] ; then
  download_dir "/labs/" "$lab"
  cd "$lab"
cat > check50 << EOF
#!/bin/sh
check50 "liv-ac-uk/comp122/2021/problems/$lab" \$@
EOF
cat > submit50 << EOF
#!/bin/sh
submit50 "liv-ac-uk/comp122/2021/problems/$lab" \$@
EOF
  chmod 750 check50 submit50
  if command -v pandoc >/dev/null && command -v pdflatex >/dev/null; then
    sed -E 's/\\/\\\\/' README.md | pandoc -o README.pdf -i -
    xdg-open ./README.pdf >/dev/null 2>&1 &
  else
    xdg-open ./README.md
  fi
fi
