#!/bin/sh

# Watch COMP122 lectures
# Requirements: curl jq rofi mpv
# sudo pacman -S --needed curl jq rofi mpv

missing() {
  echo "Missing dependency $1"
  echo "Install dependecies with one of the below commands (dependent on distro):"
  echo "\tsudo pacman -S --needed curl jq rofi mpv"
  echo "\tsudo apt-get install curl jq rofi mpv"
  exit 1
}

command -v jq >/dev/null || missing "jq"
command -v curl >/dev/null || missing "curl"
command -v rofi >/dev/null || missing "rofi"
command -v mpv >/dev/null || missing "mpv"

CANVAS_URL="https://liverpool.instructure.com"
MEDIA_URL="https://liverpool.instructuremedia.com"
COURSE_ID="19756"
PAGE_ID="lectures"

set -e

CANVAS_TOKEN=""

get_canvas_token() {
  if [ -f ~/.config/canvas_token ]; then
    CANVAS_TOKEN=$(cat ~/.config/canvas_token)
  else
    CANVAS_TOKEN=$(rofi -p "Enter canvas token: " -password -dmenu -lines 0)
    echo -n "$CANVAS_TOKEN" > ~/.config/canvas_token
    chmod 400 ~/.config/canvas_token
  fi
}

check_canvas_token() {
  curl -so /dev/null -w "%{http_code}" -H "Authorization: Bearer $1" "$CANVAS_URL/api/v1/accounts"
}

get_canvas_token
check_status=$(check_canvas_token "$CANVAS_TOKEN")
while [ "$check_status" != "200" ]; do
  rm -f ~/.config/canvas_token
  get_canvas_token
  check_status=$(check_canvas_token "$CANVAS_TOKEN")
done

lectures_page=$(curl -s -H "Authorization: Bearer $CANVAS_TOKEN" "$CANVAS_URL/api/v1/courses/$COURSE_ID/pages/$PAGE_ID" | jq -r ".body")
lectures=$(echo "$lectures_page" | tr -d "\n" | sed 's/^.*<strong>Lectures<\/strong>\(.*\)$/\1/' | grep -oE "<a [^>]*>")
lecture=$(echo "$lectures" | sed 's/^.*title="\([^"]*\)".*/\1/' | rofi -dmenu -format d -p "Lecture")
echo "$lecture"
lecture_link=$(echo "$lectures" | tail -n+${lecture} | head -n1 | sed 's/^.*data-api-endpoint="\([^"]*\)".*$/\1/')

videos_page=$(curl -s -H "Authorization: Bearer $CANVAS_TOKEN" "$lecture_link" | jq -r ".body")
iframes=$(echo "$videos_page" | tr -d "\n" | grep -oE "<iframe [^>]*>")
echo "$iframes"
video_names=""
i=1
n=$(echo "$iframes" | wc -l)
while [ "$i" -le "$n" ]; do 
  video_names="${video_names}Video $i\n"
  i=$((i + 1))
done
video=$(echo "$video_names" | rofi -dmenu -format d -p "Video")
embed_url=$(echo "$iframes" | tail -n+${video} | head -n1 | sed 's/^.*src="\([^"]*\)".*$/\1/' | sed 's/^.*url=\(.*\)$/\1/')
echo "$embed_url"

sessionless=$(curl -s -H "Authorization: Bearer $CANVAS_TOKEN" "$CANVAS_URL/api/v1/courses/$COURSE_ID/external_tools/sessionless_launch?url=$embed_url" | jq -r ".url")

iframe=$(curl -sL "$sessionless" | tr -d "\n")

form_action=$(echo "$iframe" | grep -o -E "<form[^>]*>" | sed 's/^.*action="\([^"]*\)".*$/\1/' | sed 's/&amp;/\&/')

inputs=$(echo "$iframe" | grep -o -E "<form[^>]*>.*</form>" | grep -o -E "<input[^>]*>")
data=$(echo "$inputs" | sed 's/<input[^>]*name="\([^"]*\)"[^>]*value="\([^"]*\)"[^>]*>/\1=\2\&/' | tr -d "\n" | sed 's/\&$//')

attempts=0

while [ "$attempts" -le 10 ]; do
  redirected=$(curl -s -L -o /dev/null -w "%{url_effective}" -d "$data" "$form_action" | tr -d "\n")
  view_id=$(echo -n "$redirected" | sed 's/^.*\/lti-app\/bare-embed\/\([^?]*\)?.*$/\1/')
  lti_params=$(echo -n "$redirected" | sed 's/^[^?]*?lti_params=\(.*\)$/\1/')


  if echo "$redirected" | grep -z "?lti_params=" >/dev/null; then
    launch_params=$(curl -s "$MEDIA_URL/api/lti/launch_params?lti_params=$lti_params")
    user_id=$(echo "$launch_params" | jq -r ".session.user.id")
    token=$(echo "$launch_params" | jq -r ".session.token")
    media_id=$(curl -s -H "Authorization: Bearer user_id=\"$user_id\", token=\"$token\"" "$MEDIA_URL/api/media_management/media/$view_id/lti_perspective?lti_params=$lti_params" | jq -r ".perspective_id")
    media_perspectives=$(curl -s -H "Authorization: Bearer user_id=\"$user_id\", token=\"$token\"" "$MEDIA_URL/api/media_management/perspectives/$media_id")
    video_url=$(echo "$media_perspectives" | jq -r ".perspective.media.sources[0].url")
    echo "$video_url"
    mpv "$video_url"
    break
  else
    echo "Unable to fetch video"
  fi
  attempts=$(( $attempts + 1 ))
done
