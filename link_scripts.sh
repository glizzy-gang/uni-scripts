#!/bin/sh

mkdir -p ~/.local/bin

ln -s "$PWD/Global/connect_rdp.sh" ~/.local/bin/liverpool-remote-desktop
ln -s "$PWD/COMP122/download_labs.sh" ~/.local/bin/liverpool-122-labs
ln -s "$PWD/COMP108/watch_lectures.sh" ~/.local/bin/liverpool-108-lectures
ln -s "$PWD/COMP116/watch_lectures.sh" ~/.local/bin/liverpool-116-lectures
ln -s "$PWD/COMP122/watch_lectures.sh" ~/.local/bin/liverpool-122-lectures
ln -s "$PWD/COMP124/watch_lectures.sh" ~/.local/bin/liverpool-124-lectures

echo "$PATH" | grep -z "$HOME/.local/bin" >/dev/null
if [ $? -ne 0 ]; then
  printf "%b" "\033[31mWARNING\033[0m: \"$HOME/.local/bin\" is not in your PATH\n"
fi
