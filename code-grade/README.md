# sessionless-code-crade

This is a simple rust program that uses the [Canvas LMI API][api] to open a
sessionless version of [code-grade][codegrade] in a new tab for your upcoming
assignments.

## Dependencies

- [rofi][rofi]

## Usage

1. Generate a canvas token (canvas user settings)
2. Store canvas token in file (default path: `~/.config/canvas_token`)
3. Run program :)

```
sessionless-code-grade 1.0.0
Douile [https://github.com/Douile]
Use the Canvas LMI API to open a sessionless instance of code grade for submitting assignments

USAGE:
    sessionless-code-grade [OPTIONS]

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information

OPTIONS:
    -a, --api <URL>       The URL of the Canvas LMI API to use (default https://liverpool.instructure.com/api/v1/)
    -t, --token <FILE>    The file that stores your canvas token (default ~/.config/canvas_token)
```

## Notes

- Cannot be compiled statically (`dir` dependency uses target to determine directories)

[api]: https://canvas.instructure.com/doc/api/index.html
[codegrade]: https://www.codegrade.com/
[rofi]: https://github.com/davatorium/rofi
