extern crate clap;
extern crate dirs;
extern crate reqwest;
extern crate serde;
extern crate tokio;

use std::process;
use std::{fs, io::Read};

use reqwest::header::{HeaderMap, HeaderValue, AUTHORIZATION};
use rofi::Rofi;

#[derive(Debug, serde::Deserialize)]
struct Course {
    id: u64,
    name: String,
}

#[derive(Debug, serde::Deserialize)]
struct Assignment {
    id: u64,
    name: String,
}

#[derive(Debug, serde::Deserialize)]
struct AssignmentDetails {
    url: Option<String>,
}

const API: &str = "https://liverpool.instructure.com/api/v1/";

fn default_headers(token: &str) -> HeaderMap<HeaderValue> {
    let mut headers = HeaderMap::default();
    headers.insert(AUTHORIZATION, format!("Bearer {}", token).parse().unwrap());
    headers
}

async fn open_sessionless_codegrade(
    api: &str,
    token: &str,
) -> Result<String, Box<dyn std::error::Error>> {
    let client = reqwest::Client::builder()
        .default_headers(default_headers(&token))
        .build()?;

    let courses: Vec<Course> = client
        .execute(
            client
                .get(format!(
                    "{}courses?enrollment_state=active&state=available&per_page=50",
                    api,
                ))
                .build()?,
        )
        .await?
        .json()
        .await?;

    let course = Rofi::new(
        courses
            .iter()
            .map(|c| c.name.clone())
            .collect::<Vec<String>>()
            .as_slice(),
    )
    .prompt("Course")
    .run_index()?;

    let course = courses.get(course).unwrap();
    println!("Course: {:?}", course);

    let assignments: Vec<Assignment> = client
        .execute(
            client
                .get(format!(
                    "{}courses/{}/assignments?order_by=due_at&per_page=50",
                    api, course.id
                ))
                .build()?,
        )
        .await?
        .json()
        .await?;

    let assignment = Rofi::new(
        assignments
            .iter()
            .map(|a| a.name.clone())
            .collect::<Vec<String>>()
            .as_slice(),
    )
    .prompt("Assignment")
    .run_index()?;

    let assignment = assignments.get(assignment).unwrap();
    println!("Assignment: {:?}", assignment);

    let details: AssignmentDetails = client
        .execute(
            client
                .get(format!(
                    "{}courses/{}/assignments/{}",
                    api, course.id, assignment.id
                ))
                .build()?,
        )
        .await?
        .json()
        .await?;

    let url = details.url.unwrap();

    let sessionless_url: AssignmentDetails = client
        .execute(client.get(url).build()?)
        .await?
        .json()
        .await?;

    Ok(sessionless_url.url.unwrap())
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let matches = clap::App::new(env!("CARGO_PKG_NAME"))
        .version(env!("CARGO_PKG_VERSION"))
        .author(env!("CARGO_PKG_AUTHORS"))
        .about(env!("CARGO_PKG_DESCRIPTION"))
        .arg(clap::Arg::with_name("token_file")
            .short("t")
            .long("token")
            .value_name("FILE")
            .help("The file that stores your canvas token (default ~/.config/canvas_token)")
        )
        .arg(clap::Arg::with_name("api")
            .short("a")
            .long("api")
            .value_name("URL")
            .help("The URL of the Canvas LMI API to use (default https://liverpool.instructure.com/api/v1/)")
        )
        .get_matches();

    let file_path = if let Some(file) = matches.value_of("token_file") {
        file.into()
    } else {
        dirs::config_dir().unwrap().join("canvas_token")
    };

    let mut token = String::new();
    fs::OpenOptions::new()
        .read(true)
        .open(file_path)
        .expect("Please put your canvas api token in ~/.config/canvas_token")
        .read_to_string(&mut token)?;

    let api = if let Some(api) = matches.value_of("api") {
        api
    } else {
        API
    };
    let sessionless_url = open_sessionless_codegrade(api, &token).await?;

    println!("Sessionless: {:?}", sessionless_url);
    process::Command::new("xdg-open")
        .args([sessionless_url])
        .output()
        .unwrap();

    Ok(())
}
