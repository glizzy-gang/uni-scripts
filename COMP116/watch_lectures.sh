#!/bin/sh

# Watch COMP116 lectures
# Requirements: youtube-dl mpv rofi

missing() {
  echo "Missing dependency $1"
  echo "Install dependecies with one of the below commands (dependent on distro):"
  echo "\tsudo pacman -S --needed youtube-dl mpv rofi jq"
  echo "\tsudo apt-get install youtube-dl mpv rofi jq"
  exit 1
}

command -v youtube-dl >/dev/null || missing "youtube-dl"
command -v mpv >/dev/null || missing "mpv"
command -v rofi >/dev/null || missing "rofi"
command -v jq >/dev/null || missing "jq"

playlists=$(youtube-dl --print-json --flat-playlist "https://www.youtube.com/channel/UCQEsz19Suk2uX8f29WFNrAg/playlists")

menu=$(echo "$playlists" | jq -r .title)

selected_playlist=$(echo "$menu" | rofi -dmenu -format d -p "Playlist")
if [ $? -ne 0 ]; then
  echo "No playlist selected"
  exit 0
fi

url=$(echo "$playlists" | jq -r .url | tail -n+${selected_playlist} | head -n1)
videos=$(youtube-dl --print-json --flat-playlist "$url")

menu=$(echo "$videos" | jq -r .title)

selected_video=$(echo "$menu" | rofi -dmenu -format d -p "Video")
if [ $? -ne 0 ]; then
  echo "No video selected"
  exit 0
fi

url=$(echo "$videos" | jq -r .url | tail -n+${selected_video} | head -n1)

mpv "https://youtube.com/watch?v=${url}"
