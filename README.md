# uni-scripts
_Helpful scripts for uni of Liverpool CS students_

| Scripts |
| :-----: |
| [Download a COMP122 lab](./COMP122/download_labs.sh) |
| [Connect to a remote desktop](./Global/connect_rdp.sh) |
| [Watch a COMP108 lecture](./COMP108/watch_lectures.sh) |
| [Watch a COMP116 lecture](./COMP116/watch_lectures.sh) |
| [Watch a COMP122 Lecture](./COMP122/watch_lectures.sh) |
| [Watch a COMP124 Lecture](./COMP124/watch_lectures.sh) |

If you have any of your own scripts you think might be useful for other people, please submit them via a issue or pull request

# Setting up globally
## Download the scripts
### Git clone
Clone the scripts with git, this means you can update the scripts at any time by doing a `git pull`.

```bash
git clone https://gitlab.com/glizzy-gang/uni-scripts.git
cd uni-scripts
```

### One time download
```bash
mkdir uni-scripts
cd uni-scripts
# Download the scripts you want
curl -O "https://gitlab.com/glizzy-gang/uni-scripts/-/raw/master/COMP116/watch_lectures.sh" && chmod 750 watch_lectures.sh
curl -O "https://gitlab.com/glizzy-gang/uni-scripts/-/raw/master/COMP122/download_labs.sh" && chmod 750 download_labs.sh
curl -O "https://gitlab.com/glizzy-gang/uni-scripts/-/raw/master/Global/connect_rdp.sh" && chmod 750 connect_rdp.sh
# ...
```

## Add the scripts to the path
[link\_scripts](./link_scripts.sh)

### Make sure .local/bin is in your path
- _edit ~/.profile_
- Make sure it contains a line looking like this
```
export PATH="~/.local/bin:$PATH"
```
- Note: this won't update until you logout/login or run `source ~/.profile` (bash only)

## Run any script you like 👍

